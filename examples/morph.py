from gltf import GLTF, Attributes

def main():
    model = GLTF.load("morph.gltf")
    model.repair()

    for mesh in model.meshes:
        extras = mesh.extras
        for name in extras['targetNames']:
            assert(type(name) == str)

        primitives = mesh.primitives
        for prim in primitives:
            assert(type(prim.attributes) == Attributes)
            for target in prim.targets:
                assert(type(target) == Attributes)

        weights = mesh.weights
        for weight in weights:
            assert(type(weight) == int or type(weight) == float)

    model.save("morph.out.glb")

    model_2 = GLTF.load("morph.out.glb")
    assert(model_2.render() == model.render())

if __name__ == '__main__':
    main()
