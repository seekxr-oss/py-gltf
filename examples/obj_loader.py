#!/usr/bin/env python

import argparse
import io

from gltf import GLTF, Material, Texture
from PIL import Image


def pack_image(occlusion_fp=None, metallic_fp=None, roughness_fp=None):
    blank = Image.new("L", (1024, 1024), 255)
    occlusion = Image.open(occlusion_fp).convert("L") if occlusion_fp else blank
    metallic = Image.open(metallic_fp).convert("L") if metallic_fp else blank
    roughness = Image.open(roughness_fp).convert("L") if roughness_fp else blank

    merged = Image.merge("RGB", [
        occlusion,
        metallic,
        roughness,
    ])
    merged_bytes = io.BytesIO()
    merged.save(merged_bytes, format='png')
    return merged_bytes.getvalue()


def load_obj(filename, diffuse=None, normal=None, occlusion=None,
             roughness=None, metallic=None, emissive=None):
    # Import the OBJ
    verticesObj = []
    texcoordsObj = []
    normalsObj = []
    facesObj = []
    with open(filename, 'r') as infile:
        for line in infile:
            tokens = [_ for _ in line.strip().split(' ') if _]
            if not tokens:
                continue
            datatype = tokens[0]
            if datatype == '#':
                continue
            elif datatype == "v":
                verticesObj.append([float(_) for _ in tokens[1:]])
            elif datatype == "vt":
                x, y = (float(_) for _ in tokens[1:3])
                texcoordsObj.append((x, 1.0 - y))
            elif datatype == "vn":
                normalsObj.append([float(_) for _ in tokens[1:]])
            elif datatype == "f":
                def append_face(i):
                    faces = tokens[i+1].split("/")
                    if len(faces) == 3:
                        facesObj.append((int(faces[0]), int(faces[1]) if faces[1] else None, int(faces[2]) if faces[2] else None))
                    elif len(faces) == 2:
                        facesObj.append((int(faces[0]), int(faces[1]) if faces[1] else None, None))

                index = 2
                while index <= len(tokens) - 2:
                    append_face(0)
                    append_face(index-1)
                    append_face(index)
                    index += 1
            else:
                continue

    # Load the mesh
    gltf, mesh = GLTF.simple_model('Object')
    verticesGltf = []
    texcoordsGltf = []
    normalsGltf = []
    for vertex, texture, norm in facesObj:
        verticesGltf.append(verticesObj[vertex-1])
        if texture is not None:
            texcoordsGltf.append(texcoordsObj[texture-1])
        if norm is not None:
            normalsGltf.append(normalsObj[norm-1])
    if not texcoordsGltf:
        texcoordsGltf = None
    if not normalsGltf:
        normalsGltf = None

    tex_kwargs = {}
    if diffuse:
        tex_kwargs['color_texture'] = Texture(image=diffuse)
    if normal:
        tex_kwargs['normal_texture'] = Texture(image=normal)
    if emissive:
        tex_kwargs['emissive_texture'] = Texture(image=emissive)

    packed_texture = None
    if metallic and roughness and occlusion:
        packed_texture = Texture(image=pack_image(occlusion, metallic, roughness))
    elif metallic and roughness:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, roughness_fp=roughness))
    elif metallic and occlusion:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, occlusion_fp=occlusion))
    elif roughness and occlusion:
        packed_texture = Texture(image=pack_image(roughness_fp=roughness, occlusion_fp=occlusion))

    if metallic or roughness:
        tex_kwargs['rough_metal_texture'] = packed_texture or Texture(image=metallic or roughness)
    if occlusion:
        tex_kwargs['occlusion_texture'] = packed_texture or Texture(image=occlusion)

    # Set up the material
    material = Material(
        name="PBR",
        metallic_factor=0,
        **tex_kwargs
    )

    # Finalize
    mesh.add_primitive(positions=verticesGltf,
                       texcoords=texcoordsGltf,
                       normals=normalsGltf,
                       material=material)

    return gltf


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert an obj to gltf.')
    parser.add_argument('input')
    parser.add_argument('output')

    parser.add_argument('--diffuse')
    parser.add_argument('--normal')
    parser.add_argument('--roughness')
    parser.add_argument('--occlusion')
    parser.add_argument('--metallic')
    parser.add_argument('--emissive')

    parser.add_argument('--no-embed-textures', dest='embed', action='store_false')
    parser.add_argument('--binary', action='store_true')

    parser.set_defaults(embed=True, binary=None)
    args = parser.parse_args()

    gltf = load_obj(args.input, args.diffuse, args.normal, args.occlusion,
                    args.roughness, args.metallic)

    gltf.save(args.output, binary=args.binary, embed=args.embed)
