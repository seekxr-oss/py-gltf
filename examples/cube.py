import numpy as np

from gltf import GLTF, Vec2Array


def make_cube(images, scale=1.0):
    gltf, cube = GLTF.simple_model('Picture Cube')

    vertices = np.array([
        [1, 1, 1], [-1, 1, 1], [-1, -1, 1], [1, -1, 1],
        [1, 1, -1], [-1, 1, -1], [-1, -1, -1], [1, -1, -1],
    ], dtype='float32')
    vertices *= scale

    faces = [  # indices of vertices
        [0, 1, 2,  0, 2, 3],
        [3, 2, 6,  3, 6, 7],
        [7, 6, 5,  7, 5, 4],
        [0, 4, 5,  0, 5, 1],
        [3, 7, 4,  3, 4, 0],
        [2, 1, 5,  2, 5, 6],
    ]

    uvs = Vec2Array([[1, 0], [0, 0], [0, 1], [1, 0], [0, 1], [1, 1]])
    for face, image in zip(faces, images):
        cube.add_primitive(positions=[vertices[i] for i in face],
                           texcoords=[uvs],
                           image=image)
    return gltf
