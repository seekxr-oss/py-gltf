
def export_material(name, postfix, g):
    for im in g.images:
        im.data = im.write_to('./textures')
    with open(f'{name}.mtl', 'w') as f:
        for mat in g.materials:
            if not mat.name:
                raise ValueError('Mat has no name')
            f.write(f'newmtl {mat.name}{postfix}\n')
            f.write(f'Ns {format((1 - mat.roughness_factor) * 1000, "8f")}\n')
            f.write(f'Ka 1.000000 1.000000 1.000000\n')
            f.write(f'Kd {" ".join(format(c, "8f") for c in mat.base_color_factor[:3])}\n')
            f.write(f'Ks 1.000000 1.000000 1.000000\n')
            f.write(f'Ke 0.000000 0.000000 0.000000\n')
            f.write(f'd {format(mat.base_color_factor[3], "8f")}\n')
            f.write(f'Tr {format(1 - mat.base_color_factor[3], "8f")}\n')
            f.write(f'illum 2\n')
            if mat.color_texture:
                f.write(f'map_Kd {mat.color_texture.source.path}\n')
            if mat.normal_texture:
                f.write(f'norm {mat.normal_texture.source.path}\n')
            f.write('\n')
