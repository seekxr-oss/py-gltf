#!/usr/bin/env python

import argparse
import difflib
import fnmatch
import json
import io
import os
import re

from gltf import GLTF, Material, Texture
from PIL import Image


def pack_image(occlusion_fp=None, metallic_fp=None, roughness_fp=None):
    blank = Image.new("L", (1024, 1024), 255)
    occlusion = Image.open(occlusion_fp).convert("L") if occlusion_fp else blank
    metallic = Image.open(metallic_fp).convert("L") if metallic_fp else blank
    roughness = Image.open(roughness_fp).convert("L") if roughness_fp else blank

    merged = Image.merge("RGB", [
        occlusion,
        metallic,
        roughness,
    ])
    merged_bytes = io.BytesIO()
    merged.save(merged_bytes, format='png')
    return merged_bytes.getvalue()


# creates a material given paths to the textures
def create_material(diffuse=None, normal=None, occlusion=None,
             roughness=None, metallic=None, emissive=None):
    tex_kwargs = {}
    if diffuse:
        tex_kwargs['color_texture'] = Texture(image=diffuse)
    if normal:
        tex_kwargs['normal_texture'] = Texture(image=normal)
    if emissive:
        tex_kwargs['emissive_texture'] = Texture(image=emissive)

    packed_texture = None
    if metallic and roughness and occlusion:
        packed_texture = Texture(image=pack_image(occlusion, metallic, roughness))
    elif metallic and roughness:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, roughness_fp=roughness))
    elif metallic and occlusion:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, occlusion_fp=occlusion))
    elif roughness and occlusion:
        packed_texture = Texture(image=pack_image(roughness_fp=roughness, occlusion_fp=occlusion))

    if metallic or roughness:
        tex_kwargs['rough_texture'] = packed_texture or Texture(image=metallic or roughness)
    if occlusion:
        tex_kwargs['occlusion_texture'] = packed_texture or Texture(image=occlusion)

    # Set up the material
    material = Material(
        name="PBR",
        metallic_factor=0,
        **tex_kwargs
    )
    return material


# search for a file by file name if the path is invalid
def find_file(root_folder, file_path):
    search_for_file(root_folder, file_path)

    # do we already have a valid path?
    if os.path.isfile(file_path):
        return file_path

    # get the file name
    file_name = os.path.basename(file_path)

    # search in the root folder for that file
    search = []
    for root, dirnames, filenames in os.walk(root_folder):
        for filename in fnmatch.filter(filenames, '*' + file_name + '*'):
            search.append(os.path.join(root, filename))

    if len(search) == 0:
        print("No matching files found for " + file_path + "!")
        return None
    elif len(search) > 1:
        print("Multiple files found for " + file_path + ":")
        for result in search:
            print(result)
        print("Returning the first one: " + search[0])

    return search[0]


# split a large string into words and return the percentage for the best matching word
def matches(large_string, query_string, threshold):
    # regex split it by common file name separators
    words = re.split('[ _\-.]', large_string)

    best_match = 0

    for word in words:
        s = difflib.SequenceMatcher(None, word, query_string)
        match = ''.join(word[i:i+n] for i, j, n in s.get_matching_blocks() if n)
        if len(match) / float(len(query_string)) >= threshold:
            best_match = len(match) / float(len(query_string))
    return best_match

# search in the root folder for the best matching file for a search term
def search_for_file(root_folder, search_term):
    # get all files in the root folder
    #files = list(glob.iglob(root_folder + '/**/*', recursive=True))
    files = []
    for root, dirnames, filenames in os.walk(root_folder):
        for filename in filenames:
            files.append(os.path.join(root, filename))

    # initialize best_match to the minimum threshhold for a match, 0.6 seems pretty resonable
    best_match = 0.6
    best_file_path = None

    # go through all the files and find the best match for the keyword
    for file in files:
        basename = os.path.basename(file)
        match = matches(basename, search_term, best_match)
        if match > best_match:
            best_match = match
            best_file_path = file

    # return the best one, will be None if no matches
    return best_file_path


def load_mtl(filename):
    with open(filename, 'r') as infile:
        ambient_color = []
        diffuse_color = []
        specular_color = []

        dissolve = 0.0
        specular_exponent = 0.0

        material_dict = {}
        current_material = None

        for line in infile:
            tokens = [_ for _ in line.strip().split(' ') if _]
            if not tokens:
                continue
            datatype = tokens[0]
            if datatype == '#':
                continue
            elif datatype == "newmtl":
                if current_material is not None:
                    material_dict[current_material.name] = current_material
                current_material = Material(
                    name=tokens[1],
                    metallic_factor=0,
                )
            elif datatype == "Ka":
                continue;
            elif datatype == "Kd":
                if len(tokens) == 4:
                    current_material.base_color_factor = [float(tokens[1]), float(tokens[2]), float(tokens[3]), 1.0]
                elif len(tokens) == 5:
                    current_material.base_color_factor = [float(tokens[1]), float(tokens[2]), float(tokens[3]), float(tokens[4])]
            elif datatype == "Ks":
                continue
            elif datatype == "d":
                dissolve = float(tokens[1]) if tokens[1] else 0.0
            elif datatype == "Tr":
                dissolve = 1.0 - float(tokens[1]) if tokens[1] else 0.0
            elif datatype == "Ns":
                specular_exponent = float(tokens[1]) if tokens[1] else 0.0
            elif datatype.__contains__("map"):
                # strip out everything but the file path
                line = line.replace(tokens[0] + " ", "")
                line = line.replace("\n", "")
                line = line.replace("\r", "")

                file_path = find_file(os.path.dirname(filename), line)

                if file_path is None:
                    print("Couldn't find texture file: " + line)
                    continue
                if datatype == "map_Ka":
                    current_material.occlusion_texture = Texture(image=file_path)
                if datatype == "map_Ke":
                    current_material.emissive_texture = Texture(image=file_path)
                if datatype == "map_Kd":
                    current_material.color_texture = Texture(image=file_path)
                if datatype == "map_Ks":
                    current_material.color_texture = Texture(image=file_path)
                if datatype == "map_Ns":
                    current_material.rough_texture = Texture(image=file_path)
                if datatype == "map_Bump":
                    current_material.normal_texture = Texture(image=file_path)
            else:
                continue

        if current_material is not None:
            material_dict[current_material.name] = current_material

    return material_dict


# searches the folder for the best matches for each material type and assigns them to a new material
def find_textures_and_make_material(root_folder):
    new_material = Material(
        name="PBR",
        metallic_factor=0,
    )

    diffuse = search_for_file(root_folder, "diffuse")
    if diffuse:
        new_material.color_texture = Texture(image=diffuse)

    normal = search_for_file(root_folder, "normal")
    if normal:
        new_material.normal_texture = Texture(image=normal)

    emissive = search_for_file(root_folder, "emissive")
    if emissive:
        new_material.emissive_texture = Texture(image=emissive)

    occlusion = search_for_file(root_folder, "occlusion")
    metallic = search_for_file(root_folder, "specular")
    roughness = search_for_file(root_folder, "roughness")

    packed_texture = None
    if metallic and roughness and occlusion:
        packed_texture = Texture(image=pack_image(occlusion, metallic, roughness))
    elif metallic and roughness:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, roughness_fp=roughness))
    elif metallic and occlusion:
        packed_texture = Texture(image=pack_image(metallic_fp=metallic, occlusion_fp=occlusion))
    elif roughness and occlusion:
        packed_texture = Texture(image=pack_image(roughness_fp=roughness, occlusion_fp=occlusion))

    if metallic or roughness:
        new_material.rough_texture = packed_texture or Texture(image=metallic or roughness)
    if occlusion:
        new_material.occlusion_texture = packed_texture or Texture(image=occlusion)

    material_dict = {"default": new_material}
    return material_dict



def load_obj(root_folder, diffuse=None, normal=None, occlusion=None,
             roughness=None, metallic=None, emissive=None):
    # get the gltf and mesh ready to be added to first
    gltf, mesh = GLTF.simple_model('Object')

    # Import the OBJ
    verticesObj = []
    texcoordsObj = []
    normalsObj = []
    facesObj = []
    currentMtl = None
    material_dict = None

    filename = find_file(root_folder, ".obj")

    with open(filename, 'r') as infile:
        for line in infile:
            tokens = [_ for _ in line.strip().split(' ') if _]
            if not tokens:
                continue
            datatype = tokens[0]
            if datatype == '#':
                continue
            elif datatype == "v":
                verticesObj.append([float(_) for _ in tokens[1:]])
            elif datatype == "vt":
                x, y = (float(_) for _ in tokens[1:3])
                texcoordsObj.append((x, 1.0 - y))
            elif datatype == "vn":
                print(tokens)
                normalsObj.append([float(_) for _ in tokens[1:]])
            elif datatype == "f":
                def append_face(i):
                    faces = tokens[i+1].split("/")
                    if len(faces) == 3:
                        facesObj.append((int(faces[0]), int(faces[1]) if faces[1] else None, int(faces[2]) if faces[2] else None))
                    elif len(faces) == 2:
                        facesObj.append((int(faces[0]), int(faces[1]) if faces[1] else None, None))

                index = 2
                while index <= len(tokens) - 2:
                    append_face(0)
                    append_face(index-1)
                    append_face(index)
                    index += 1
            #TODO: create materials from the specified mtl file
            elif datatype == "mtllib":
                # strip out everything but the file path
                line = line.replace(tokens[0] + " ", "")
                line = line.replace("\n", "")
                line = line.replace("\r", "")

                file_path = find_file(os.path.dirname(filename), line)

                # load the mtl if it exists
                if file_path:
                    print("loading mtl file from " + file_path)
                    material_dict = load_mtl(file_path)
                # if we don't have the mtl, we'll need to improvise
                else:
                    print("couldn't find mtl, trying to assemble material starting in " + root_folder)
                    material_dict = find_textures_and_make_material(root_folder)
                continue

            # switching materials, make the current data into a primitive and add it to the mesh
            #TODO: combine sections using the same material into one primitive?
            elif datatype == "usemtl":
                if currentMtl:
                    # Load the mesh
                    verticesGltf = []
                    texcoordsGltf = []
                    normalsGltf = []

                    for vertex, texture, norm in facesObj:
                        if vertex is not None:
                            verticesGltf.append(verticesObj[vertex - 1])
                        if texture is not None:
                            texcoordsGltf.append(texcoordsObj[texture - 1])
                        if norm is not None:
                            normalsGltf.append(normalsObj[norm - 1])
                    if not texcoordsGltf:
                        texcoordsGltf = None
                    if not normalsGltf:
                        normalsGltf = None

                    # Finalize
                    mesh.add_primitive(positions=verticesGltf,
                                       texcoords=texcoordsGltf,
                                       normals=normalsGltf,
                                       material=currentMtl)
                facesObj = []
                if tokens[1] in material_dict:
                    currentMtl = material_dict[tokens[1]]
                else:
                    currentMtl = material_dict["default"]
                continue
            else:
                continue

    # take care of the last mesh if it exists
    # some objs finish by having an extra usemtl line, but not all of them do
    if len(facesObj) > 0:
        # Load the mesh
        verticesGltf = []
        texcoordsGltf = []
        normalsGltf = []

        for vertex, texture, norm in facesObj:
            if vertex is not None:
                verticesGltf.append(verticesObj[vertex - 1])
            if texture is not None:
                texcoordsGltf.append(texcoordsObj[texture - 1])
            if norm is not None:
                normalsGltf.append(normalsObj[norm - 1])
        if not texcoordsGltf:
            texcoordsGltf = None
        if not normalsGltf:
            normalsGltf = None

        # Finalize
        mesh.add_primitive(positions=verticesGltf,
                           texcoords=texcoordsGltf,
                           normals=normalsGltf,
                           material=currentMtl)

    return gltf


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert an obj to gltf.')
    parser.add_argument('input')
    parser.add_argument('output')

    parser.add_argument('--diffuse')
    parser.add_argument('--normal')
    parser.add_argument('--roughness')
    parser.add_argument('--occlusion')
    parser.add_argument('--metallic')
    parser.add_argument('--emissive')

    parser.add_argument('--no-embed-textures', dest='embed', action='store_false')

    parser.set_defaults(embed=True)
    args = parser.parse_args()

    gltf = load_obj(args.input, args.diffuse, args.normal, args.occlusion,
                    args.roughness, args.metallic)

    with open(args.output, 'w') as f:
        json.dump(gltf.render(embed=args.embed), f)
